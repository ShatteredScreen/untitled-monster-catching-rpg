extends 'res://addons/gut/test.gd'

var sut

func before_each():
  sut = partial_double(MenuServiceImpl).new()
  add_child_autofree(sut)

func test_it_should_add_an_item_to_the_menu():
  # Given an id
  var expected_id = 'Save'

  # And a page control
  var expected_page = autofree(Panel.new())

  var menu_spy = autofree(double(Menu).new())
  stub(sut, '_get_menu').to_return(menu_spy)

  # When we add a menu item
  sut.add_menu_item(expected_id, expected_page)

  # Then the menu should have a new item
  assert_called(menu_spy, 'add_item', [ expected_id, expected_page, null ])