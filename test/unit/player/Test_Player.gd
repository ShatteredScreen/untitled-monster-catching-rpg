extends 'res://addons/gut/test.gd'

var sut
var mock_menu_service
var mock_menu

func before_each():
  mock_menu_service = double(MenuServiceImpl).new()
  mock_menu = double(Menu).new() 
  
  var scene = add_child_autofree(load('res://test/unit/player/Test_Player.tscn').instance())
  sut = scene.find_node('Player')

  sut.menu_service = mock_menu_service
  stub(sut.menu_service, '_get_menu').to_return(mock_menu)
  
  add_child_autofree(sut)

func test_it_should_move(params = use_parameters(ParameterFactory.named_parameters(
  [ 'action',           'x',    'z'   ], [
  [ 'player_left',      -0.18,  0.0   ],
  [ 'player_right',     0.18,   0.21  ],
  [ 'player_forward',   0.0,    -0.0  ],
  [ 'player_backward',  0.0,    0.0   ]
]))):
  # Given an action
  var expected_action = params.action

  # And intended updated position
  var expected_x = params.x
  var expected_z = params.z

  # And an input event action
  var expected_input_event = InputEventAction.new()

  # When action pressed
  expected_input_event.action = expected_action
  expected_input_event.pressed = true
  sut._input(expected_input_event)
  yield(yield_for(0.2), YIELD)

  expected_input_event.pressed = false
  sut._input(expected_input_event)
  yield(yield_for(0.2), YIELD)
  var actual = sut.translation

  # Then the players position has been updated
  assert_almost_eq(actual.x, expected_x, 0.3)
  assert_almost_eq(actual.z, expected_z, 0.3)

func test_it_should_fall_the_the_ground():
  # Given a starting position
  var staring_position = Vector3.UP * 5

  # When the player is in the air
  sut.velocity = sut.move_and_slide(staring_position, Vector3.UP)
  yield(yield_for(0.5), YIELD)

  # Then the player is on the floor
  assert_true(sut.is_on_floor())

func test_it_should_be_busy_when_the_menu_is_open():
  # Given the player is not busy
  sut.busy = false

  # When the menu opens
  yield(yield_for(0.1), YIELD)
  mock_menu.emit_signal('opened')
  yield(yield_for(0.1), YIELD)

  # Then the player is busy
  assert_true(sut.busy)

func test_it_should_no_longer_be_busy_when_the_menu_is_closed():
  # Given the player is busy
  sut.busy = true

  # When the menu closes
  yield(yield_for(0.1), YIELD)
  mock_menu.emit_signal('closed')
  yield(yield_for(0.1), YIELD)

  # Then the player is busy
  assert_false(sut.busy)