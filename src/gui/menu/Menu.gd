extends Control
class_name Menu

# Observable signal that is emitted when the items of the menu are updated
signal items_updated(menu_items)

# Observable signal that is emitted when the menu is opened
signal opened

# Observable signal that is emitted when the menu is closed
signal closed

onready var _items_button_container: VBoxContainer = $Panel/ItemsButtonContainer
onready var _items_page_container: Control = $ItemsPageContainer

var _open: bool
var _items: Array = []

# Create a new instance of a Menu
# 
# @param  open  The initial state of the menu when it enters the scene tree
func _init(open: bool = false) -> void:
  _open = open

# Add an item to the menu
# 
# The position of the items in the menu dictates which order their buttons will be displayed
# 
# @param  id        The unique id of the menu item
# @param  page      The page control that will be displayed when the item is active
# @param  position  The position in of the item in the list of items
# 
# @return The updated menu instance
func add_item(id: String, page: Control, position: int = -1) -> Menu:
  _items.append(MenuItem.new(id, page, position))
  emit_signal('items_updated', _items)
  return self

# Open the menu
# 
# This will make the menu visible, aswell as change the state of the menu to open, 
# and emit the associated signal.
func open() -> void:
  _open = true
  emit_signal('opened')
  show()
  
  # Focus the first button
  var first_button = _items_button_container.get_child(0)
  if first_button is Button:
    first_button.grab_focus()

# Close the menu
# 
# This will make the menu hidden, aswell as change the state of the menu to closed, 
# and emit the associated signal.
func close() -> void:
  _open = false
  emit_signal('closed')
  hide()

# Check if the menu is currently open
# 
# @return The open state of the menu
func is_open() -> bool:
  return _open

# Lifecycle hook for when the node enters the scene tree
# 
# Depending on the initial open state of the menu, it will hide the menu.
func _ready() -> void:
  if !_open:
    hide()

  # warning-ignore:return_value_discarded
  connect('items_updated', self, '_on_items_updated', [], CONNECT_DEFERRED)

# Lifecycle hook for handling input events
# 
# Handle the inputs for opening and closing the menu.
# 
# @param  event The currently handled input event
func _input(event: InputEvent) -> void:
  _handle_visibility_input(event)

# Handle the input events for opening and closing the menu
# 
# @param  event The currently handled input event
func _handle_visibility_input(event: InputEvent) -> void:
  if event.is_action_pressed('ui_menu'):
    if _open:
      close()
    else:
      open()

# Hook for when the items of the menu are updated
# 
# It will update the display of the buttons and pages of the menu
# 
# @param  items The items of the menu
func _on_items_updated(items: Array) -> void:
  # Add all non existing items to the container
  for item in items:
    item = item as MenuItem

    # Add button
    if !_items_button_container.has_node(item.button_name):
      var button = Button.new()
      button.name = item.button_name
      button.text = item.id
      _items_button_container.add_child(button)
      
    # Add page
    if !_items_page_container.has_node(item.page_name):
      item.page.name = item.page_name
      _items_page_container.add_child(item.page)
  
  # Sort the items
  var sorted_items = items
  sorted_items.sort_custom(self, '_sort_items')

  # Sort the item buttons in the container accordingly
  for item in sorted_items:
    item = item as MenuItem
    var index = sorted_items.find(item)
    var existing_item = _items_button_container.get_node(item.button_name)
    if existing_item && existing_item.get_index() != index:
      _items_button_container.move_child(existing_item, index)

# Sort the items of the menu based on their position
# 
# @param  a The left side of the comparison
# @param  b The right side of the comparison
#
# @return `TRUE` if the left side is lesser, else it returns `FALSE` if
#         the left side is greater
func _sort_items(a: MenuItem, b: MenuItem) -> bool:
  return a.position < b.position
