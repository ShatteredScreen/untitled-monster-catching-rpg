extends 'res://addons/gut/test.gd'

func test_it_should_get_the_button_name():
  # Given the id
  var expected_id = 'Foo'

  # And the page
  var expected_page = autofree(Panel.new())

  # And the position
  var expected_position = 1

  # When we get the button name
  var sut = MenuItem.new(expected_id, expected_page, expected_position)
  autofree(sut)
  var actual = sut.button_name
    
  # Then it should equal "${ID}Button"
  assert_eq(actual, 'FooButton')

func test_it_should_get_the_page_name():
 # Given the id
  var expected_id = 'Foo'

  # And the page
  var expected_page = autofree(Panel.new())

  # And the position
  var expected_position = 1

  # When we get the page name
  var sut = MenuItem.new(expected_id, expected_page, expected_position)
  autofree(sut)
  var actual = sut.page_name
    
  # Then it should equal "${ID}Button"
  assert_eq(actual, 'FooPage')