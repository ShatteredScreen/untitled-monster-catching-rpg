extends Control
class_name DialogueBox

# Observable signal for when the dialogue tree has been started
signal started(dialogue)

# Observable signal for when the dialogue tree moves to the next line 
signal next_line(line)

# Observable signal for when the dialogue tree has finished
signal finished(dialogue)

# Label containing the current line of the dialogue tree
onready var text_label: RichTextLabel = $PanelContainer/GridContainer/Text

# Label containing the name of the speaker of the current line in the dialogue tree
onready var speaker_label: RichTextLabel = $PanelContainer/GridContainer/Speaker

# Container for the next action
onready var action_container: Container = $PanelContainer/GridContainer/ActionContainer

# Container for the response actions
onready var option_container: Container = $PanelContainer/GridContainer/OptionContainer

var _dialogue: ClydeDialogue

# Lifecycle event for when an instance is create
#
# Hide the initial display of the instance and setup connections to signals
func _ready() -> void:
  hide()

  connect('started', self, '_on_started', [], CONNECT_DEFERRED)
  connect('next_line', self, '_on_next_line', [], CONNECT_DEFERRED)
  connect('finished', self, '_on_finished', [], CONNECT_DEFERRED)

# Lifecycle event for handling input events
#
# Bind the handling for selecting and selecting an action
#
# @param  event The currently handled input event
func _input(event: InputEvent) -> void:
  _handle_change_focus_input(event)
  _handle_accept_input(event)

# Lifecycle event for when an option button is pressed
# 
# Chose option based on the index of the button that was pressed
#
# @param  index Index of the option that has been selected
func _on_option_button_pressed(index: int) -> void:
  choose(index)

# Lifecycle event for when the next button is pressed
#
# Progresses the dialogue tree to the next line
func _on_next_button_pressed() -> void:
  next()

# Lifecycle event for when the dialogue tree has started
#
# Show the currently hidden instance
#
# @param  dialogue  The dialogue tree that has been started
func _on_started(dialogue: ClydeDialogue) -> void:
  show()

# Lifecycle event for when the next line of the dialogue tree has been started
#
# Update the content of the node
#
# @param  line  Content of the next line of the dialogue tree
func _on_next_line(line: Dictionary) -> void:
  update_content(line)

# Lifecycle event for when the dialogue tree has finished
#
# Hides the node upon completion
#
# @param  dialogue  The completed dialogue tree
func _on_finished(dialogue: ClydeDialogue) -> void:
  hide()

# Handle the accept input and click the currently focused button
#
# @param  event The currently handled input event
func _handle_accept_input(event: InputEvent) -> void:
  var focused_button = get_focus_owner()
  if event.is_action_pressed('ui_accept') && focused_button:
    focused_button.emit_signal('pressed')

# Handle the navigation input and change the focus of 
# the currently selected button
#
# @param  event The currently handled input event
func _handle_change_focus_input(event: InputEvent) -> void:
  var focused_button = get_focus_owner()

  if event.is_action('ui_up'):
    focused_button.focus_neighbour_top()

  elif event.is_action('ui_down'):
    focused_button.focus_neighbour_bottom()

# Start the provided dialogue tree
#
# @param  dialogue  The provided dialogue tree that will be started
#
# @return The current instance of the node
func start(dialogue: ClydeDialogue) -> DialogueBox:
  _dialogue = dialogue
  _dialogue.start()
  emit_signal('started', _dialogue)
  next()
  return self

# Progress to the next line in the dialogue tree
#
# If there is no futher content, it will finish the dialogue tree
#
# @return The current instance of the node
func next() -> DialogueBox:
  var content = _dialogue.get_content()

  if content:
    emit_signal('next_line', content)
    return self

  return finish()

# Choose an option at a fork in the dialogue tree before continuing to
# the next line
#
# @param  index The index of the selected option
#
# @return The current instance of the node
func choose(index: int) -> DialogueBox:
  _dialogue.choose(index)
  return next()

# Complete the dialogue tree
#
# @return The current instance of the node
func finish() -> DialogueBox:
  emit_signal('finished', _dialogue)
  return self

# Update the text and actions of the dialogue box with new content
#
# @param  content The content of the new dialogue line
func update_content(content: Dictionary) -> void:
  _update_text(content)
  _update_actions(content)  

# Update the text of the dialogue box
#
# @param  content the new incoming line
func _update_text(content: Dictionary) -> void:
  # Clear the controls text
  speaker_label.clear()
  text_label.clear()

  speaker_label.append_bbcode(content.speaker)

  match content.type:
    'line':
      text_label.append_bbcode(content.text)

    'options':
      text_label.append_bbcode(content.name)

# Update the actions for the next line of the dialogue tree
#
# @param  content The content of the new dialogue line
func _update_actions(content: Dictionary) -> void:
  # Remove the action controls
  _free_children(option_container)
  _free_children(action_container)

  var focused_control

  match content.type:
    'line':
      var next_button = Button.new()
      next_button.name = 'NextButton'
      next_button.text = 'NEXT'
      next_button.connect('pressed', self, '_on_next_button_pressed')
      action_container.add_child(next_button)
      focused_control = next_button

    'options':
      for index in len(content.options):
        var option = content.options[index]
        var option_button = Button.new()
        option_button.name = 'Option{index}Button'.format({ 'index': index })
        option_button.text = option.label
        option_button.connect('pressed', self, '_on_option_button_pressed', [ index ])
        option_container.add_child(option_button)

      var option_buttons = option_container.get_children()
      var options_length = len(option_buttons)
      for index in options_length:
        var neighbour_top_index = index - 1
        var neighbour_bottom_index = 0 if index == (options_length - 1) else index + 1

        var button = option_container.get_child(index) as Button
        button.focus_neighbour_top = option_buttons[neighbour_top_index].get_path()
        button.focus_neighbour_bottom = option_buttons[neighbour_bottom_index].get_path()

      focused_control = option_container.get_child(0)
  
  focused_control.grab_focus()

# Free any child nodes of a parent
#
# @param  parent  The parent node
func _free_children(parent: Node) -> void:
  for child in parent.get_children():
    child.queue_free()
