RSRC                    PackedDataContainer                                                                       resource_local_to_scene    resource_name 	   __data__    script        
   local://1 �          PackedDataContainer          B  {"type":"document","content":[{"type":"content","content":[{"type":"line","value":"Hello darkness my old : friend 123!","id":null,"speaker":null,"tags":null},{"type":"line","value":"I come to show you this line with id","id":"SCENE_1_LINE002","speaker":null,"tags":null}]}],"blocks":[{"type":"block","name":"this_is_a_block","content":{"type":"content","content":[{"type":"line","value":"This is a dialogue with speaker and id","id":"SCENE_1_LINE001","speaker":"NPC","tags":null},{"type":"line","value":"This is a line with another speaker","id":null,"speaker":"player","tags":null},{"type":"options","name":null,"content":[{"type":"option","name":"this is the first topic","mode":"once","content":{"type":"content","content":[{"type":"line","value":"this is the first topic","id":null,"speaker":null,"tags":null},{"type":"line","value":"just a normal line inside the topic","id":null,"speaker":"NPC","tags":["sad","happy","some_tag"]},{"type":"line","value":"another line inside the first topic","id":null,"speaker":"player","tags":null}]},"id":null,"speaker":null,"tags":null},{"type":"option","name":"the second topic","mode":"once","content":{"type":"content","content":[{"type":"line","value":"the second topic","id":null,"speaker":null,"tags":null},{"type":"line","value":"a line in the second topic, with speaker defined.","id":null,"speaker":"player","tags":null}]},"id":null,"speaker":null,"tags":null}],"id":null,"speaker":null,"tags":null},{"type":"line","value":"Those are the topics available:","id":null,"speaker":null,"tags":null},{"type":"options","name":null,"content":[{"type":"option","name":"another topic","mode":"once","content":{"type":"content","content":[{"type":"line","value":"another topic","id":null,"speaker":null,"tags":null},{"type":"line","value":"line inside a topic","id":null,"speaker":"player","tags":null},{"type":"line","value":"another line inside a topic","id":null,"speaker":"NPC","tags":null},{"type":"line","value":"a line without speaker inside the topic","id":null,"speaker":null,"tags":null},{"type":"line","value":"a line with id inside the topic","id":"this_is_an_id","speaker":null,"tags":null},{"type":"divert","target":"<parent>"}]},"id":null,"speaker":null,"tags":null},{"type":"option","name":"one more topic","mode":"once","content":{"type":"content","content":[{"type":"line","value":"one more topic","id":null,"speaker":null,"tags":null},{"type":"line","value":"love is all you need","id":null,"speaker":"speaker","tags":null},{"type":"divert","target":"<parent>"}]},"id":null,"speaker":null,"tags":null},{"type":"option","name":"this is a topic pointing to another block","mode":"once","content":{"type":"content","content":[{"type":"line","value":"this is a topic pointing to another block","id":null,"speaker":null,"tags":null},{"type":"divert","target":"another_block"}]},"id":null,"speaker":null,"tags":null},{"type":"option","name":"this is a sticky topic, with redirect to the beginning of the topics block","mode":"sticky","content":{"type":"content","content":[{"type":"line","value":"this is a sticky topic, with redirect to the beginning of the topics block","id":null,"speaker":null,"tags":null},{"type":"line","value":"just a line","id":null,"speaker":null,"tags":null},{"type":"divert","target":"<parent>"}]},"id":null,"speaker":null,"tags":null}],"id":null,"speaker":null,"tags":null}]}},{"type":"block","name":"another_block","content":{"type":"content","content":[{"type":"options","name":null,"content":[{"type":"option","name":"straight to the topic","mode":"once","content":{"type":"content","content":[{"type":"line","value":"straight to the topic","id":null,"speaker":null,"tags":null},{"type":"line","value":"Go, johnny go.","id":null,"speaker":"player","tags":null}]},"id":null,"speaker":null,"tags":null}],"id":null,"speaker":null,"tags":null},{"type":"divert","target":"<parent>"},{"type":"variations","mode":"sequence","content":[{"type":"content","content":[{"type":"line","value":"This is the first time I talk to you","id":null,"speaker":null,"tags":null}]},{"type":"content","content":[{"type":"line","value":"This is the second time I talk to you","id":null,"speaker":null,"tags":null}]},{"type":"content","content":[{"type":"variations","mode":"sequence","content":[{"type":"content","content":[{"type":"line","value":"Well, I lost count already.","id":null,"speaker":null,"tags":null}]}]}]}]},{"type":"variations","mode":"sequence","content":[{"type":"content","content":[{"type":"divert","target":"money_talk"}]},{"type":"content","content":[{"type":"divert","target":"crazy_talk"}]},{"type":"content","content":[{"type":"divert","target":"drunk_talk"}]}]},{"type":"variations","mode":"once","content":[{"type":"content","content":[{"type":"line","value":"This is the first time I talk to you","id":null,"speaker":null,"tags":null}]},{"type":"content","content":[{"type":"line","value":"This is the second time I talk to you","id":null,"speaker":null,"tags":null}]},{"type":"content","content":[{"type":"line","value":"Well, I lost count already.","id":null,"speaker":null,"tags":null}]}]},{"type":"variations","mode":"cycle","content":[{"type":"content","content":[{"type":"line","value":"This is the first time I talk to you","id":null,"speaker":null,"tags":null}]},{"type":"content","content":[{"type":"line","value":"This is the second time I talk to you","id":null,"speaker":null,"tags":null}]},{"type":"content","content":[{"type":"line","value":"Well, I lost count already.","id":null,"speaker":null,"tags":null}]}]},{"type":"variations","mode":"shuffle","content":[{"type":"content","content":[{"type":"line","value":"This is the first time I talk to you","id":null,"speaker":null,"tags":["test","tag"]}]},{"type":"content","content":[{"type":"line","value":"This is the second time I talk to you |test|","id":null,"speaker":null,"tags":null}]},{"type":"content","content":[{"type":"line","value":"Well, I lost count already.","id":null,"speaker":null,"tags":null}]}]},{"type":"variations","mode":"shuffle once","content":[{"type":"content","content":[{"type":"line","value":"This is the first time I talk to you","id":null,"speaker":null,"tags":null}]},{"type":"content","content":[{"type":"line","value":"This is the second time I talk to you","id":null,"speaker":null,"tags":null}]},{"type":"content","content":[{"type":"line","value":"Well, I lost count already.","id":null,"speaker":null,"tags":null}]}]},{"type":"line","value":"-> this is the way\\nheyhey","id":null,"speaker":null,"tags":null},{"type":"line","value":"speaker: this is $id: the way\\nheyhey","id":"whatsup","speaker":null,"tags":null},{"type":"line","value":"speaker: this is the way\\nheyhey","id":null,"speaker":null,"tags":null},{"type":"line","value":"this is the way\\nheyhey","id":null,"speaker":null,"tags":null}]}},{"type":"block","name":"money_talk","content":{"type":"content","content":[{"type":"line","value":"blah","id":null,"speaker":null,"tags":null}]}},{"type":"block","name":"crazy_talk","content":{"type":"content","content":[{"type":"line","value":"bleh","id":null,"speaker":null,"tags":null}]}},{"type":"block","name":"drunk_talk","content":{"type":"content","content":[{"type":"line","value":"wat","id":null,"speaker":null,"tags":null}]}}]}  RSRC