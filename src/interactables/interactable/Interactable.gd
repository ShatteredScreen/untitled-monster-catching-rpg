extends Node
class_name Interactable

# Observable signal for when interacted with
signal interact(trigger)

# Determine if the focus indicator is displayed when focused
export(bool) var display_focus_indicator = true

# Determine if interactable is currently focused
var focused: bool setget _set_focused, _get_focused

onready var _focus_indicator: Sprite3D = $FocusIndicator
onready var _range: Area = $Range

var _focused: bool = false

# Lifecycle hook for when an instance is created
#
# Register all connections to internal signals
func _ready() -> void:
  # Add to group
  add_to_group('Interactables')

  # Register listeners for signals
  _range.connect('area_entered', self, '_on_area_enter')
  _range.connect('area_exited', self, '_on_area_exited')

  _update_focus_indicator()

# Trigger the interaction.
#
# Emits the interact signal.
# 
# @param  trigger The object triggering the interaction
func interact(trigger: Node) -> void:
  emit_signal('interact', trigger)

# Lifecycle hook for when an area enters the area.
#
# When an `InteractableRange` enters the area focus this instance.
#
# @param  area  The area entering the area
func _on_area_enter(area: Area) -> void:
  if _is_interactable_range(area):
    area.add_interactable(self)
    self.focused = true

# Lifecycle hook for when an area exits the area.
#
# When an `InteractableRange` exits the area unfocus this instance.
#
# @param  area  The area exiting the area
func _on_area_exited(area: Area) -> void:
  if _is_interactable_range(area):
    area.remove_interactable(self)
    self.focused = false

# Update the display of the focus indicator to match the state
# of this instance's focus.
func _update_focus_indicator() -> void:
  if display_focus_indicator:
    _focus_indicator.visible = self._focused

# Determine if an instance of an `Area` is an instance of `InteractableRanges`.
#
# @todo When Godot 4.0 comes out and unbreaks the static typing, replace this
#       with a simpler static type check.
func _is_interactable_range(area: Area) -> bool:
  return area.is_in_group('InteractableRanges') \
    && area.has_method('add_interactable') \
    && area.has_method('remove_interactable')

# Set the focus state
#
# @param  value The incoming focus value
func _set_focused(value: bool) -> void:
  # Unfocus all other instances of `Interactable` in the tree
  if value:
    var interactables = get_tree().get_nodes_in_group('Interactables')
    for interactable in interactables:
      if interactable != self:
        interactable.focused = false

  # Focus the current instance and update the state of the indicator
  _focused = value
  _update_focus_indicator()

# Get the focus state
func _get_focused() -> bool:
  return _focused
