extends 'res://addons/gut/test.gd'

var scene
var sut

func before_each():
  scene = add_child_autofree(load('res://test/unit/gui/dialogue/dialogue-box/Test_DialogueBox.tscn').instance())
  sut = scene.find_node('DialogueBox')
  
func test_it_should_start_the_dialogue_tree():
  var expected_dialogue = double(ClydeDialogue).new()
  var expected_content = { 
    'type': 'line', 
    'speaker': 'Test', 
    'text': 'Hello World' 
  }   
  
  stub(expected_dialogue, 'get_content').to_return(expected_content)
  
  watch_signals(sut)
  
  var actual = sut.start(expected_dialogue)
  yield(yield_for(0.1), YIELD)
  
  assert_called(expected_dialogue, 'start')
  assert_signal_emitted_with_parameters(sut, 'started', [ expected_dialogue ])
  assert_is(actual, DialogueBox)
  assert_eq(actual, sut)
  assert_true(sut.visible)
  
func test_it_should_move_to_the_next_line_of_the_dialogue_tree(params = use_parameters(ParameterFactory.named_parameters(
  [ 'content'                                                                                                     ], [
  [ { 'type': 'line', 'speaker': 'Test', 'text': 'Hello World' }                                                  ],
  [ { 'type': 'options', 'speaker': 'Test', 'name': 'Choose and option', 'option': [ { 'label': 'Goodbye!' } ] }  ]
]))):
  var expected_dialogue = double(ClydeDialogue).new()
  var expected_content = params.content
  
  watch_signals(sut)
  
  stub(expected_dialogue, 'get_content').to_return(expected_content)
  
  var actual = sut.start(expected_dialogue).next()
  
  assert_called(expected_dialogue, 'get_content')
  assert_signal_emitted_with_parameters(sut, 'next_line', [ expected_content ])
  assert_is(actual, DialogueBox)
  assert_eq(actual, sut)
  
func test_it_should_choose_an_option_from_the_dialogue_tree():
  var expected_dialogue = double(ClydeDialogue).new()
  var expected_content = null
  var expected_index = 0
  
  watch_signals(sut)
  
  stub(expected_dialogue, 'get_content').to_return(expected_content)
  
  var actual = sut.start(expected_dialogue).choose(expected_index)
  
  assert_called(expected_dialogue, 'choose', [ expected_index ])
  
func test_it_should_finish_the_dialogue_tree():
  var expected_dialogue = double(ClydeDialogue).new()
  var expected_content = null
  
  watch_signals(sut)
  
  stub(expected_dialogue, 'get_content').to_return(expected_content)
  
  var actual = sut.start(expected_dialogue).next()
  
  assert_signal_emitted_with_parameters(sut, 'finished', [ expected_dialogue ])
  assert_is(actual, DialogueBox)
  assert_eq(actual, sut)
  
func test_it_should_display_the_speaker_and_the_text(params = use_parameters(ParameterFactory.named_parameters(
  [ 'content',                                                                                                                            'speaker',  'text'              ], [
  [ { 'type': 'line', 'speaker': 'Test', 'text': 'Hello World' },                                                                         'Test',     'Hello World'       ],
  [ { 'type': 'options', 'speaker': 'Test', 'name': 'Choose an option', 'options': [ { 'label': 'No Way!' }, { 'label': 'Goodbye!' } ] }, 'Test',     'Choose an option'  ]
]))):
  var expected_content = params.content
  var expected_speaker = params.speaker
  var expected_text = params.text
  
  sut.update_content(expected_content)
  
  assert_eq(sut.speaker_label.text, expected_speaker)
  assert_eq(sut.text_label.text, expected_text)
  
func test_it_should_display_the_actions(params = use_parameters(ParameterFactory.named_parameters(
  [ 'content',                                                                                                                            'actions'                             ], [
  [ { 'type': 'line', 'speaker': 'Test', 'text': 'Hello World' },                                                                         [ 'NextButton' ]                      ],
  [ { 'type': 'options', 'speaker': 'Test', 'name': 'Choose an option', 'options': [ { 'label': 'No Way!' }, { 'label': 'Goodbye!' } ] }, [ 'Option0Button', 'Option1Button' ]  ]
]))):
  var expected_content = params.content
  var expected_actions = params.actions
  
  sut.update_content(expected_content)
  yield(yield_for(0.1), YIELD)
  
  for expected_action in expected_actions:
    if sut.find_node(expected_action, true, false) == null:
      fail_test(
        'Unable to find an action with the expected name of {action_name}'\
          .format({ 'action_name': expected_action })
      )
  
  pass_test('Found all expected actions')
  
