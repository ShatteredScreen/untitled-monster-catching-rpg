extends KinematicBody
class_name Player

# Movement speed of the player
export(float) var movement_speed = 100

# Rate of acceleration for the player's movement speed
export(float) var acceleration = 0.1

# Amount of friction applied when the player tries to stop
export(float) var friction = 0.4

# Current velocity the player is traveling at
var velocity: Vector3 setget _set_velocity, _get_velocity

# If the player is currently in the process of interacting with something
var busy: bool setget _set_busy, _get_busy

onready var menu_service: MenuServiceImpl = get_node('/root/MenuService')

onready var _interactable_range: InteractableRange = $InteractableRange
onready var _gravity = -ProjectSettings.get_setting('physics/3d/default_gravity')

var _input_direction: Vector2
var _horizontal_velocity: Vector2
var _vertical_velocity: float
var _busy: bool = false

# Apply movement to the player
#
# @param  delta The number of seconds since the last synched frame of physics
func move(delta: float) -> void:
  # Apply horizontal force
  var input_velocity = (_input_direction.normalized() * movement_speed) * delta
  if input_velocity.length() > 0:
    _horizontal_velocity = _horizontal_velocity.linear_interpolate(input_velocity, acceleration)

  else:
    _horizontal_velocity = _horizontal_velocity.linear_interpolate(Vector2.ZERO, friction)

  # Apply gavitational force
  if !is_on_floor():
    _vertical_velocity += delta * _gravity

  self.velocity = move_and_slide(
    self.velocity,
    Vector3.UP,
    true,
    4,
    deg2rad(45)
  )

# Lifecycle hook for when an instance is created.
#
# Adds the instance to the `Players` group.
func _ready() -> void:
  add_to_group('Players')
  add_to_group('Busiable')

  call_deferred('_handle_busying_events')

# Lifecycle hook for processing the inputs of the player
#
# @param  event The currently handled input event
func _input(event: InputEvent) -> void:
  if !self.busy:
    _handle_use_input(event)
    _handle_movement_input(event)

# Lifecycle hook for processing the physics of an object.
#
# Process the movement of the player, synched with the physics.
#
# @param  delta The number of seconds since the last synched frame of physics
func _physics_process(delta: float) -> void:
  move(delta)

# Handle events that would cause the player to become busy
# 
# When the menu is open busy the player so that the controls for manipulating the menu
# don't also move the player.
func _handle_busying_events() -> void:
  # warning-ignore:return_value_discarded
  menu_service.menu.connect('opened', self, '_set_busy', [ true ], CONNECT_DEFERRED)
  # warning-ignore:return_value_discarded
  menu_service.menu.connect('closed', self, '_set_busy', [ false ], CONNECT_DEFERRED)

# Handle the input events for moving the player
#
# @param  event The currently handled input event
func _handle_movement_input(event: InputEvent) -> void:
  if event.is_action('player_forward'):
    _input_direction.y = -event.get_action_strength('player_forward')

  elif event.is_action('player_backward'):
    _input_direction.y = event.get_action_strength('player_backward')

  if event.is_action('player_left'):
    _input_direction.x = -event.get_action_strength('player_left')

  elif event.is_action('player_right'):
    _input_direction.x = event.get_action_strength('player_right')

# Handle the input events for interacting with `Interactables`
#
# @param  event The currently handled input event
func _handle_use_input(event: InputEvent) -> void:
  if event.is_action_pressed('player_use'):
    if _interactable_range.focused_interactable:
      _interactable_range.focused_interactable.interact(self)

# Set the horizontal velocity of the player
#
# @param  value The incoming horizontal vector
func _set_velocity(value: Vector3) -> void:
  _horizontal_velocity.x = value.x
  _horizontal_velocity.y = value.z

# Get the current velocity of the player
func _get_velocity() -> Vector3:
  return Vector3(
    _horizontal_velocity.x,
    _vertical_velocity,
    _horizontal_velocity.y
  )

# Set the busy state of the player
# 
# @param  value The incoming busy state
func _set_busy(value: bool) -> void:
  _busy = value

# Get the current busy state of the player
func _get_busy() -> bool:
  return _busy
