extends Node
class_name DialogueServiceImpl

const DialogueBoxScene = preload('res://src/gui/dialogue/dialogue-box/DialogueBox.tscn')

# Dialogue box rendered in the Scene by the service
var dialogue_box setget , _get_dialogue_box

# Start a dialogue tree
# 
# @param  dialogue  The dialogue that will be started
# 
# @return           The dialogue box rendered in the scene
func start(dialogue: ClydeDialogue) -> DialogueBox:
  # Ensure that a dialogue box exists
  return self.dialogue_box.start(dialogue)

# Get the dialogue box rendered in the scene.
# 
# If a dialogue box does not exist, one will be created
func _get_dialogue_box() -> DialogueBox:
  var value = get_node_or_null('/root/DialogueBox')

  if !value:
    value = DialogueBoxScene.instance()
    value.name = 'DialogueBox'
    get_tree().root.add_child(value)

  return value as DialogueBox
