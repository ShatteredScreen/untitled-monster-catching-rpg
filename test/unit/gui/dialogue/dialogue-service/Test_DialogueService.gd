extends 'res://addons/gut/test.gd'

var sut

func before_each():
  sut = DialogueServiceImpl.new()
  add_child_autofree(sut)
  
func test_it_should_start_a_dialogue_tree():
  var expected_dialogue = double(ClydeDialogue).new()
  
  var actual = sut.start(expected_dialogue)
  
  assert_not_null(actual)
  assert_is(actual, DialogueBox)
