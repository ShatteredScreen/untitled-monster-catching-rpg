extends 'res://addons/gut/test.gd'

var scene
var sut

func before_each():
  scene = add_child_autofree(load('res://test/unit/interactables/interactable/Test_Interactable.tscn').instance())
  sut = scene.find_node('Interactable')

func test_it_should_be_focused_when_an_interactable_range_enters():
  var expected_interactable_range = scene.find_node('InteractableRange')
  var expected_translation = Vector3(0, 0, 0)

  expected_interactable_range.translation = expected_translation
  yield(yield_for(0.1), YIELD)

  assert_true(sut.focused)

func test_it_should_not_be_focused_when_an_interactable_range_enters():
  var expected_interactable_range = scene.find_node('InteractableRange')
  var expected_entered_translation = Vector3(0, 0, 0)
  var expected_exited_translation = Vector3(1, 0, 0)

  expected_interactable_range.translation = expected_entered_translation
  yield(yield_for(0.1), YIELD)

  expected_interactable_range.translation = expected_exited_translation
  yield(yield_for(0.1), YIELD)

  assert_false(sut.focused)

var test_it_should_display_the_focus_indicator_params = ParameterFactory.named_parameters(
  [ 'focused',  'indicator_enabled',  'result'  ], [
  [ true,       true,                 true      ],
  [ false,      true,                 false     ],
  [ true,       false,                false     ],
  [ false,      false,                false     ],
])
func test_it_should_display_the_focus_indicator(params = use_parameters(test_it_should_display_the_focus_indicator_params)):
  var expected_focus = params.focused
  var expected_indicator_enabled = params.indicator_enabled
  var expected_result = params.result

  sut.display_focus_indicator = expected_indicator_enabled
  sut.focused = expected_focus

  yield(yield_for(0.1), YIELD)
  var focus_indicator = sut.get_node('FocusIndicator')
  
  assert_eq(focus_indicator.visible, expected_result)
