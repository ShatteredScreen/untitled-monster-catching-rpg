extends 'res://addons/gut/test.gd'

var scene
var sut

func before_each():
  scene = add_child_autofree(load('res://test/unit/interactables/readable/Test_Readable.tscn').instance())
  sut = scene.find_node('Readable')
  sut.dialogue_service = double(DialogueServiceImpl).new()
  add_child_autofree(sut)

func test_it_should_make_the_interaction_source_busy():
  # Given a source
  var expected_source = autofree(Player.new())

  # And a dialogue box
  var expected_dialogue_box = double(DialogueBox).new()
  stub(sut.dialogue_service, 'start').to_return(expected_dialogue_box)

  # When we interact with the readable
  expected_source._ready()
  sut.emit_signal('interact', expected_source)

  # Then the source is busy
  assert_true(expected_source.busy)

  # And the dialogue is started
  assert_called(sut.dialogue_service, 'start')

func test_it_should_make_the_interaction_source_not_busy():
  # Given a source
  var expected_source = autofree(Player.new())
  
  # And a dialogue box 
  var expected_dialogue_box = double(DialogueBox).new()
  stub(sut.dialogue_service, 'start').to_return(expected_dialogue_box)

  # When we interact with the readable
  expected_source._ready()
  sut.emit_signal('interact', expected_source)
  expected_dialogue_box.emit_signal('finished', expected_source)

  # Then the source is not busy
  assert_false(expected_source.busy)
