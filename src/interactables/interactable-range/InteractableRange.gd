extends Area
class_name InteractableRange

# The currently focused `Interactable` within range
var focused_interactable: Interactable setget , _get_focused_interactable

# The collection of `Interactable` in range
var interactables: Array = []

# Lifecycle hook for when an instance is created.
#
# Adds the instance to the `InteractableRanges` group.
func _ready() -> void:
  add_to_group('InteractableRanges')

# Lifecycle hook for processing at a fixed time rate.
#
# @param  delta The number of seconds since the last frame
func _process(delta: float) -> void:
  focus_closest_interactable()

# Focus the closest interactable within the player's range.
func focus_closest_interactable() -> void:
  # Sort the interactables by distance from the player
  if !interactables.empty():
    var sorted_interactables = interactables
    sorted_interactables.sort_custom(self, '_sort_closest')

    # If the closest interactable is not focused, then focus it
    var closest_interactable = sorted_interactables[0]
    if closest_interactable && !closest_interactable.focused:
      closest_interactable.focused = true

# Append an interactable to the collection of interactables
# within range.
#
# @param  value Interactable to be added
func add_interactable(value: Interactable) -> void:
  interactables.append(value)

# Remove an interactable from the collection of interactables
# within range.
#
# @param  value Interactable to be removed
func remove_interactable(value: Interactable) -> void:
   interactables.erase(value)

# Sort a collection of `Spatial` based on how close it it to the current node
#
# @param  a The left side of the comparison
# @param  b The right side of the comparison
#
# @return `TRUE` if the left side is closer, else it returns `FALSE` if
#         the right side is closer
func _sort_closest(a: Spatial, b: Spatial) -> bool:
  var position = global_transform.origin
  var distance_to_a = position.distance_squared_to(a.global_transform.origin)
  var distance_to_b = position.distance_squared_to(b.global_transform.origin)

  return distance_to_a < distance_to_b

# Get the focused interactable
func _get_focused_interactable() -> Interactable:
  if !interactables.empty():
    for interactable in interactables:
      if interactable.focused:
        return interactable

  return null
