extends 'res://addons/gut/test.gd'

var scene
var sut

func before_each():
  scene = add_child_autofree(load('res://test/unit/interactables/interactable-range/Test_InteractableRange.tscn').instance())
  sut = scene.find_node('InteractableRange')

func test_it_should_add_an_interactable():
  var expected_interactable = scene.find_node('ClosestInteractable')

  sut.add_interactable(expected_interactable)

  assert_has(sut.interactables, expected_interactable)

func test_it_should_remove_an_interactable():
  var expected_interactable = scene.find_node('ClosestInteractable')

  sut.add_interactable(expected_interactable)
  sut.remove_interactable(expected_interactable)

  assert_does_not_have(sut.interactables, expected_interactable)

func test_it_should_focus_the_closest_interactable():
  var expected_closer_interactable = scene.find_node('ClosestInteractable')
  var expected_farther_interactable = scene.find_node('FarthestInteractable')

  sut.add_interactable(expected_closer_interactable)
  sut.add_interactable(expected_farther_interactable)

  sut.focus_closest_interactable()

  assert_eq(sut.focused_interactable, expected_closer_interactable)

func test_it_should_get_the_focused_interactable():
  var expected_interactable = scene.find_node('ClosestInteractable')

  sut.add_interactable(expected_interactable)
  expected_interactable.focused = true

  assert_eq(sut.focused_interactable, expected_interactable)
