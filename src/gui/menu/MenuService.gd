extends Node
class_name MenuServiceImpl

const MenuScene = preload('res://src/gui/menu/Menu.tscn')

# Menu rendered in the Scene by the service
var menu: Control setget , _get_menu

# Add an item to the menu
# 
# @param  id    The unique id of the menu item
# @param  page  The page that will be displayed when the item is active
func add_menu_item(id: String, page: Control) -> void:
  self.menu.add_item(id, page)

# Lifecycle hook for when the node is autoloaded
# 
# Asynchronously set up the initial menu items
func _ready() -> void:
  call_deferred('_load_default_items')

# Hook for setting up the default items of the menu
func _load_default_items() -> void:
  add_menu_item('Quests', Panel.new())
  add_menu_item('Save', Panel.new())
  add_menu_item('Creatures', Panel.new())
  add_menu_item('Options', Panel.new())

# Get the currently rendered menu
# 
# If there is no menu in the scene it will create one
func _get_menu() -> Menu:
  var value = get_node_or_null('/root/Menu')

  if !value:
    value = MenuScene.instance()
    value.name = 'Menu'
    get_tree().root.add_child(value)

  return value as Menu

