extends Object
class_name MenuItem

const DEFAULT_POSITION: int = -1

# Position of the menu item in the list
var position: int = DEFAULT_POSITION

# Unique id of the menu item used to determine the localized name
var id: String

# Control that will be displayed when the item is active
var page: Control

# Name of the button in the scene tree
var button_name: String setget , _get_button_name

# Name of the page in the scene tree
var page_name: String setget , _get_page_name

# Create a new instance of a MenuItem
# 
# @param  initialId       Unique id of the menu item that will be used for localization
# @param  initialPage  Control that will be displayed when the item is active
# @param  initialPosition Position of the item in the menu
func _init(initialId: String, initialPage: Control, initialPosition: int = DEFAULT_POSITION) -> void:
  id = initialId
  page = initialPage

  if initialPosition != DEFAULT_POSITION:
    position = initialPosition

# Get the name of the menu item's button
func _get_button_name() -> String:
  return '{id}Button'.format({ 'id': id })

# Get the name of the menu item's page
func _get_page_name() -> String:
  return '{id}Page'.format({ 'id': id })