extends Interactable
class_name Readable

onready var dialogue_service: DialogueServiceImpl = get_node('/root/DialogueService')

# Path to the dialogue resource
export(String, FILE) var content_path: String

var _dialogue: ClydeDialogue = ClydeDialogue.new()

# Lifecycle hook for when an instance is created
# 
# Register all the signal listeners and prepare the dialogue
func _ready() -> void:
  
  # Prepare the dialogue
  _dialogue.load_dialogue(content_path)

  # Register listeners for signals
  connect('interact', self, '_on_interact_started')

# Lifecycle hook for when the interaction has started
# 
# It starts the dialogue and marks the source of the interaction as busy
# 
# @param  source  The node that triggered the interaction
func _on_interact_started(source: Node) -> void:
  var dialogue_box = dialogue_service.start(_dialogue)
  dialogue_box.connect('finished', self, '_on_interact_finished', [ source ])

  if source.is_in_group('Busiable'):
    source.busy = true

# Lifecycle hook for when the interaction has finished
# 
# It makes the source of the ineteraction available again for interactions
# 
# @param  dialogue  The dialogue that finished
# @param  source    The node that triggered the interation
func _on_interact_finished(dialogue: ClydeDialogue, source: Node) -> void:
  _dialogue = dialogue
  if source.is_in_group('Busiable'):
    source.busy = false
