extends 'res://addons/gut/test.gd'

var sut

func before_each():
  var scene = add_child_autofree(load('res://test/unit/gui/menu/Test_Menu.tscn').instance())
  sut = scene.find_node('Menu')

func test_it_should_add_an_item_to_the_menu():
  # Given an id
  var expected_id = 'Save'

  # And a page
  var expected_page = autofree(Panel.new())

  # And a position
  var expected_position = 1

  # When we add an item
  watch_signals(sut)
  var actual = sut.add_item(expected_id, expected_page, expected_position)
  yield(yield_for(0.1), YIELD)

  # Then it returns the updated menu
  assert_is(actual, Menu)
  assert_eq(actual, sut)

  # And emit the updated items
  assert_signal_emitted(sut, 'items_updated')

  # And add the button
  assert(sut.get_node('Panel/ItemsButtonContainer/SaveButton'))

  # And add the panel
  assert(sut.get_node('ItemsPageContainer/SavePage'))

func test_it_should_open_the_menu():
  # When we open the menu
  watch_signals(sut)
  sut.open()

  # Then it should be open
  assert_true(sut.visible)

  # And emit opened
  assert_signal_emitted(sut, 'opened')

func test_it_should_close_the_menu():
  # When we close the menu
  watch_signals(sut)
  sut.close()

  # Then it should be closed
  assert_false(sut.visible)

  # And emit closed
  assert_signal_emitted(sut, 'closed')